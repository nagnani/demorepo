import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SearchboxComponent } from './components/searchbox/searchbox.component';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductPageComponent } from './components/product-page/product-page.component';
import { HomePostCardComponent } from './components/home-post-card/home-post-card.component';
import { HomePageComponent } from './components/home-page/home-page.component';

const routes: Routes = [
  {
    path: 'products/:id/:productUrlName',
    component: ProductPageComponent
  },
  {
    path: '',
    component: HomePageComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SearchboxComponent,
    ProductPageComponent,
    HomePostCardComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes/* ,
      { enableTracing: true } */)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
