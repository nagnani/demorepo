export interface Product {
  user?: any;
  createdDate: number[];
  lastModifiedBy?: any;
  lastModifiedDate: number[];
  isActive: boolean;
  version: number;
  id: number;
  name: string;
  description: string;
  category: string;
  quantity: Quantity;
  image: Image;
  urlName: string;
  new: boolean;
}

export interface Image {
  thumbnail: Thumbnail[];
  images: Thumbnail[];
  medium: Thumbnail[];
}

export interface Thumbnail {
  imageUrl: string;
  primary: boolean;
}

export interface Quantity {
  numerical: Number[];
}

export interface Number {
  qt: string;
  active: boolean;
}
