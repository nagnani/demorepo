import { NgModule } from '@angular/core';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';

@NgModule ({
    imports: [MatButtonModule, MatMenuModule, MatIconModule, MatInputModule, MatAutocompleteModule,
      MatToolbarModule, MatCardModule],
    exports: [MatButtonModule, MatMenuModule, MatIconModule, MatInputModule, MatAutocompleteModule,
      MatToolbarModule, MatCardModule]
})
export class MaterialModule { }
