import { Injectable } from '@angular/core';
import { HttpParams } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { createHttpObservable } from 'src/app/common/util';
import { Product } from '../model/product';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor() { }

  public fetchProducts(): Observable<Product[]> {

    return createHttpObservable(
      `/api/products`);
  }

  public fetchProduct(id: string): Observable<Product> {

    return createHttpObservable(
      `/api/products/${id}`);
  }

  public searchResults(inputData: string): Observable<Product[]> {

    return of(inputData)
        .pipe(
          debounceTime(400),
          distinctUntilChanged(),
          switchMap(search => this.loadProducts(search))
        );
  }

  private loadProducts(search = ''): Observable<Product[]> {

      return createHttpObservable(`/api/search?query=${search}`);
  }
  
}
