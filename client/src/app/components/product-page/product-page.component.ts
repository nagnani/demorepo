import { Product } from 'src/app/model/product';
import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { UrlSegment, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-product-page',
  templateUrl: './product-page.component.html',
  styleUrls: ['./product-page.component.css']
})
export class ProductPageComponent implements OnInit {

  seg: UrlSegment[];
  id: string;
  product$: Observable<Product>;

  constructor(private route: ActivatedRoute,
              private productService: ProductService) {

                this.seg = this.route.snapshot.url;
                this.id = this.seg[1].path;
               }

  ngOnInit() {

    this.product$ = this.productService.fetchProduct(this.id);
    /* .subscribe(pro => {
      this.product = pro;
     } ); */
    console.log(JSON.stringify(this.product$));
  }

}
