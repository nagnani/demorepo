import { Component, OnInit, Input } from '@angular/core';
import { Product } from 'src/app/model/product';

@Component({
  selector: 'app-home-post-card',
  templateUrl: './home-post-card.component.html',
  styleUrls: ['./home-post-card.component.css']
})
export class HomePostCardComponent implements OnInit {

  @Input()
  product: Product;

  constructor() { }

  ngOnInit() {
  }

  loadProduct(product: Product) {
    window.location.href = 'products/' + product.id + '/' + product.urlName;
  }

}
