import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import { Product } from 'src/app/model/product';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-searchbox',
  templateUrl: './searchbox.component.html',
  styleUrls: ['./searchbox.component.css']
})
export class SearchboxComponent implements OnInit {

  retrivedProducts$: Observable<Product[]>;
  myControl = new FormControl('', Validators.minLength(1));

  constructor(private productService: ProductService) { }

  ngOnInit() {
  }

  getSearchProducts(inputData: string) {
    
    this.retrivedProducts$ = this.productService.searchResults(inputData);
  }

  loadProduct(product: Product) {
    window.location.href = 'products/' + product.id + '/' + product.urlName;
  }

}
